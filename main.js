document.addEventListener('DOMContentLoaded', () => {

const container = document.createElement('div');
container.id = 'container-fluid';
container.classList.add('container')
container.style.marginTop = '50px'

const inputForm = document.createElement('form');
inputForm.id = 'inputForm';
inputForm.style.display = 'flex';
inputForm.style.flexDirection = 'column'

const divSurname = document.createElement('div');
const labelSurname = document.createElement('label');
labelSurname.classList.add('form-label');
labelSurname.for = 'inputSurname';
labelSurname.textContent = 'Фамилия';
const inputSurname = document.createElement('input');
inputSurname.classList.add('form-control')
inputSurname.id = 'inputSurname';
divSurname.append(labelSurname, inputSurname);

const divName = document.createElement('div');
const labelName = document.createElement('label');
labelName.classList.add('form-label');
labelName.for = 'inputName';
labelName.textContent = 'Имя';
const inputName = document.createElement('input');
inputName.classList.add('form-control')
inputName.id = 'inputName';
divName.append(labelName, inputName);

const divMidname = document.createElement('div');
const labelMidname = document.createElement('label');
labelMidname.classList.add('form-label');
labelMidname.for = 'inputMidname';
labelMidname.textContent = 'Отчество';
const inputMidname = document.createElement('input');
inputMidname.classList.add('form-control')
inputMidname.id = 'inputMidname';
divMidname.append(labelMidname, inputMidname);

const divDateOfBirth = document.createElement('div');
const labelDateOfBirth = document.createElement('label');
labelDateOfBirth.classList.add('form-label');
labelDateOfBirth.for = 'inputDateOfBirth';
labelDateOfBirth.textContent = 'Дата рождения';
const inputDateOfBirth = document.createElement('input');
inputDateOfBirth.classList.add('form-control')
inputDateOfBirth.id = 'inputDateOfBirth';
divDateOfBirth.append(labelDateOfBirth, inputDateOfBirth);
inputDateOfBirth.type = 'date';

const divStartOfStudy = document.createElement('div');
const labelStartOfStudy = document.createElement('label');
labelStartOfStudy.classList.add('form-label');
labelStartOfStudy.for = 'inputStartOfStudy';
labelStartOfStudy.textContent = 'Год начала обучения';
const inputStartOfStudy = document.createElement('input');
inputStartOfStudy.classList.add('form-control')
inputStartOfStudy.id = 'inputStartOfStudy';
divStartOfStudy.append(labelStartOfStudy, inputStartOfStudy);
inputStartOfStudy.type = 'number';
inputStartOfStudy.min = '2000';

const divFaculty = document.createElement('div');
const labelFaculty = document.createElement('label');
labelFaculty.classList.add('form-label');
labelFaculty.for = 'inputFaculty';
labelFaculty.textContent = 'Факультет';
const inputFaculty = document.createElement('input');
inputFaculty.classList.add('form-control')
inputFaculty.id = 'inputFaculty';
divFaculty.append(labelFaculty, inputFaculty);

const inputFeilds = document.createElement('div');
inputFeilds.classList.add('inputFeilds', 'mb-3')
inputFeilds.style.display = 'flex';
inputFeilds.style.alignItems = 'baseline'
inputFeilds.style.gap = '5px 5px';

inputFeilds.append(divSurname, divName, divMidname, divDateOfBirth, divStartOfStudy, divFaculty)

const formButton = document.createElement('button');
formButton.type = 'submit';
formButton.classList.add('btn', 'btn-primary', 'mb-5', 'col-3');
formButton.textContent = 'Добавить';
formButton.style.marginLeft = 'auto';
formButton.style.marginRight = 'auto';

inputForm.append(inputFeilds, formButton);

const table = document.createElement('table');
table.classList.add('table')
const tr = document.createElement('tr');
tr.style.cursor = 'pointer'
const tdName = document.createElement('td');
tdName.textContent = 'Имя';
const tdSurname = document.createElement('td');
tdSurname.textContent = 'Фамилия';
const tdMidname = document.createElement('td');
tdMidname.textContent = 'Отчество';
const tdDateOFBirth = document.createElement('td');
tdDateOFBirth.textContent = 'Дата рожения';
const tdStartOfStudy = document.createElement('td');
tdStartOfStudy.textContent = 'Год начала обучения';
const tdFaculty = document.createElement('td');
tdFaculty.textContent = 'Факультет';

//////////////////////// Форма фильтрации
///////////////////////
const inputFormFilter = document.createElement('form');
inputFormFilter.id = 'inputFormFilter';
inputFormFilter.style.display = 'flex';
inputFormFilter.style.flexDirection = 'column'

const inputSurnameFilter = document.createElement('input');
inputSurnameFilter.classList.add('form-control')
inputSurnameFilter.id = 'inputSurnameFilter';

const inputNameFilter = document.createElement('input');
inputNameFilter.classList.add('form-control')
inputNameFilter.id = 'inputNameFilter';

const inputMidnameFilter = document.createElement('input');
inputMidnameFilter.classList.add('form-control')
inputMidnameFilter.id = 'inputMidnameFilter';

const inputDateOfBirthFilter = document.createElement('input');
inputDateOfBirthFilter.classList.add('form-control')
inputDateOfBirthFilter.id = 'inputDateOfBirthFilter';
inputDateOfBirthFilter.type = 'date';

const inputStartOfStudyFilter = document.createElement('input');
inputStartOfStudyFilter.classList.add('form-control')
inputStartOfStudyFilter.id = 'inputStartOfStudyFilter';
inputStartOfStudyFilter.type = 'number';
inputStartOfStudyFilter.min = '2000';

const inputFacultyFilter = document.createElement('input');
inputFacultyFilter.classList.add('form-control')
inputFacultyFilter.id = 'inputFacultyFilter';

const inputFeildsFilter = document.createElement('div');
inputFeildsFilter.classList.add('inputFeildsFilter', 'mb-3')
inputFeildsFilter.style.display = 'flex';
inputFeildsFilter.style.alignItems = 'baseline'
inputFeildsFilter.style.gap = '5px 5px';

const formButtonFilter = document.createElement('button');
formButtonFilter.type = 'submit';
formButtonFilter.classList.add('btn', 'btn-outline-secondary', 'col-3');
formButtonFilter.textContent = 'Фильтр';
formButtonFilter.style.marginLeft = 'auto';
formButtonFilter.style.marginRight = 'auto';

inputFeildsFilter.append(inputSurnameFilter, inputNameFilter, inputMidnameFilter, inputDateOfBirthFilter, inputStartOfStudyFilter, inputFacultyFilter)
inputFormFilter.append(inputFeildsFilter, formButtonFilter)
///////////////////////
///////////////////////

tr.append(tdSurname, tdName, tdMidname, tdDateOFBirth, tdStartOfStudy, tdFaculty);
table.append(tr)

container.append(inputForm);
container.append(inputFormFilter);
container.append(table);
document.body.append(container)

let trNewRow;
let tdNewName;
let tdNewSurname;
let tdNewMidName;
let tdNewDateOfBirth;
let tdNewStartOfStudy;
let tdNewFaculty;
let student = [];
let studentArray = [];
let studentArrayFiltered = [];
const todayDate = new Date();
let studentFiltered;
let clickCount = 0;
//ФУНКЦИЯ КНОПКИ///////////////////////////////////////////////////////
formButton.addEventListener('click', (e) => {
  e.preventDefault();
  trNewRow = document.createElement('tr');
  tdNewName = document.createElement('td');
  tdNewSurname = document.createElement('td');
  tdNewMidName = document.createElement('td');
  tdNewDateOfBirth = document.createElement('td');
  tdNewStartOfStudy = document.createElement('td');
  tdNewFaculty = document.createElement('td');
  studentObj = {};

  inputName.style.borderColor = '#ced4da';
  inputSurname.style.borderColor = '#ced4da';
  inputMidname.style.borderColor = '#ced4da';
  inputFaculty.style.borderColor = '#ced4da';
  inputStartOfStudy.style.borderColor = '#ced4da'
  inputDateOfBirth.style.borderColor = '#ced4da';

  if (/\d/.test(inputName.value) || /\d/.test(inputSurname.value) || /\d/.test(inputMidname.value) || /\d/.test(inputFaculty.value)) {
    alert('Не надо писать тут цифры')
    inputName.style.borderColor = 'red';
    inputSurname.style.borderColor = 'red';
    inputMidname.style.borderColor = 'red';
    inputFaculty.style.borderColor = 'red';
    return
  }

  if (inputName.value.trim().length == 0 ||
  inputSurname.value.trim().length == 0 ||
  inputMidname.value.trim().length == 0 ||
  inputDateOfBirth.value.trim().length == 0 ||
  inputStartOfStudy.value.trim().length == 0 ||
  inputFaculty.value.trim().length == 0
  ) {
    alert ('Надо заполнить все поля')
    return
  }

  if (parseInt(inputDateOfBirth.value) < 1900-01-01) {
    alert('Вы слишком старый');
    inputDateOfBirth.style.borderColor = 'red';
    return
  }

  if (parseInt(inputDateOfBirth.value) > todayDate.getFullYear()) {
    alert ('Для нас, вы еще не родились');
    inputDateOfBirth.style.borderColor = 'red';
    return
  }

  if (parseInt(inputStartOfStudy.value) < 2000 || parseInt(inputStartOfStudy.value) > todayDate.getFullYear()) {
    alert('быть того не может, либо нас тогда не было, либо вы из будущего');
    inputStartOfStudy.style.borderColor = 'red';
    return
  }

  studentObj.name = inputName.value[0].toUpperCase() + inputName.value.slice(1);
  studentObj.surname = inputSurname.value[0].toUpperCase() + inputSurname.value.slice(1);
  studentObj.midname = inputMidname.value[0].toUpperCase() + inputMidname.value.slice(1);
  studentObj.dateofbirth = inputDateOfBirth.value;
  studentObj.startofstudy = inputStartOfStudy.value;
  studentObj.faculty = inputFaculty.value;

  student.push(studentObj);
  for (let i = 0; i<student.length; i++) {

    tdNewName.textContent = student[i].name;
    tdNewSurname.textContent = student[i].surname;
    tdNewMidName.textContent = student[i].midname;
    tdNewDateOfBirth.textContent = new Date(student[i].dateofbirth).toLocaleDateString();
    if ((todayDate.getFullYear() - parseInt(student[i].startofstudy)) > 4) {
      tdNewStartOfStudy.textContent = `${student[i].startofstudy} - ${parseInt(student[i].startofstudy) + 4} (закончил)`;
    }
    else {
      tdNewStartOfStudy.textContent = `${student[i].startofstudy} - ${parseInt(student[i].startofstudy) + 4} (${todayDate.getFullYear() - parseInt(student[i].startofstudy)} курс)`;
    }
    tdNewFaculty.textContent = student[i].faculty;
  }


  //student.push(trNewRow);

  trNewRow.append(tdNewSurname, tdNewName, tdNewMidName, tdNewDateOfBirth, tdNewStartOfStudy, tdNewFaculty);
  table.append(trNewRow);
  studentArray.push(trNewRow);
  inputForm.reset();
  console.log(student)
});
///////////////////////////////////////////////////////////////
function listReplacer () {
  studentArray.forEach (el => el.remove());
  for (let x = 0; x < student.length; x++) {
    let trNewRow1 = document.createElement('tr');
    let tdNewName1 = document.createElement('td');
    let tdNewSurname1 = document.createElement('td');
    let tdNewMidName1 = document.createElement('td');
    let tdNewDateOfBirth1 = document.createElement('td');
    let tdNewStartOfStudy1 = document.createElement('td');
    let tdNewFaculty1 = document.createElement('td');

    tdNewName1.textContent = student[x].name;
    tdNewSurname1.textContent = student[x].surname;
    tdNewMidName1.textContent = student[x].midname;
    tdNewDateOfBirth1.textContent = new Date(student[x].dateofbirth).toLocaleDateString();
    if ((todayDate.getFullYear() - parseInt(student[x].startofstudy)) > 4) {
      tdNewStartOfStudy1.textContent = `${student[x].startofstudy} - ${parseInt(student[x].startofstudy) + 4} (закончил)`;
    }
    else {
      tdNewStartOfStudy1.textContent = `${student[x].startofstudy} - ${parseInt(student[x].startofstudy) + 4} (${todayDate.getFullYear() - parseInt(student[x].startofstudy)} курс)`;
    }
    tdNewFaculty1.textContent = student[x].faculty;

    trNewRow1.append(tdNewSurname1, tdNewName1, tdNewMidName1, tdNewDateOfBirth1, tdNewStartOfStudy1, tdNewFaculty1);
    studentArrayFiltered.push(trNewRow1);

    table.append(trNewRow1);
  };
}

function listReplacerFilter () {
  studentArray.forEach (item => item.remove());
  studentArrayFiltered.forEach (item => item.remove());
  for (let x = 0; x < studentFiltered.length; x++) {
    let trNewRow1 = document.createElement('tr');
    let tdNewName1 = document.createElement('td');
    let tdNewSurname1 = document.createElement('td');
    let tdNewMidName1 = document.createElement('td');
    let tdNewDateOfBirth1 = document.createElement('td');
    let tdNewStartOfStudy1 = document.createElement('td');
    let tdNewFaculty1 = document.createElement('td');

    tdNewName1.textContent = studentFiltered[x].name;
    tdNewSurname1.textContent = studentFiltered[x].surname;
    tdNewMidName1.textContent = studentFiltered[x].midname;
    tdNewDateOfBirth1.textContent = new Date(studentFiltered[x].dateofbirth).toLocaleDateString();
    if ((todayDate.getFullYear() - parseInt(studentFiltered[x].startofstudy)) > 4) {
      tdNewStartOfStudy1.textContent = `${studentFiltered[x].startofstudy} - ${parseInt(studentFiltered[x].startofstudy) + 4} (закончил)`;
    }
    else {
      tdNewStartOfStudy1.textContent = `${studentFiltered[x].startofstudy} - ${parseInt(studentFiltered[x].startofstudy) + 4} (${todayDate.getFullYear() - parseInt(studentFiltered[x].startofstudy)} курс)`;
    }
    tdNewFaculty1.textContent = studentFiltered[x].faculty;

    trNewRow1.append(tdNewSurname1, tdNewName1, tdNewMidName1, tdNewDateOfBirth1, tdNewStartOfStudy1, tdNewFaculty1);
    studentArrayFiltered.push(trNewRow1);

    table.append(trNewRow1);
  };
}

tdSurname.addEventListener ('click', () => {
++clickCount;
studentArrayFiltered.forEach (elem => elem.remove());
if (clickCount == 1) {
  student.sort(function (a, b) {
    if (a.surname > b.surname) {
      return 1;
    }
    if (a.surname < b.surname) {
      return -1;
    }
    return 0;
  });
  listReplacer();
}
if (clickCount == 2) {
  student.sort(function (a, b) {
    if (a.surname > b.surname) {
      return -1;
    }
    if (a.surname < b.surname) {
      return 1;
    }
    return 0;
  });
  listReplacer();
  clickCount = 0;
}
});

tdName.addEventListener ('click', () => {
  ++clickCount;
  studentArrayFiltered.forEach (elem => elem.remove());
  if (clickCount == 1) {
    student.sort(function (a, b) {
      if (a.name > b.name) {
        return 1;
      }
      if (a.name < b.name) {
        return -1;
      }
      return 0;
    });
    listReplacer();
  }
  if (clickCount == 2) {
    student.sort(function (a, b) {
      if (a.name > b.name) {
        return -1;
      }
      if (a.name < b.name) {
        return 1;
      }
      return 0;
    });
    listReplacer();
    clickCount = 0;
  }
});

tdMidname.addEventListener ('click', () => {
  ++clickCount;
  studentArrayFiltered.forEach (elem => elem.remove());
  if (clickCount == 1) {
    student.sort(function (a, b) {
      if (a.midname > b.midname) {
        return 1;
      }
      if (a.midname < b.midname) {
        return -1;
      }
      return 0;
    });
    listReplacer();
  }
  if (clickCount == 2) {
    student.sort(function (a, b) {
      if (a.midname > b.midname) {
        return -1;
      }
      if (a.midname < b.midname) {
        return 1;
      }
      return 0;
    });
    listReplacer();
    clickCount = 0;
  }
});

tdFaculty.addEventListener ('click', () => {
  ++clickCount;
  studentArrayFiltered.forEach (elem => elem.remove());
  if (clickCount == 1) {
    student.sort(function (a, b) {
      if (a.faculty > b.faculty) {
        return 1;
      }
      if (a.faculty < b.faculty) {
        return -1;
      }
      return 0;
    });
    listReplacer();
  }
  if (clickCount == 2) {
    student.sort(function (a, b) {
      if (a.faculty > b.faculty) {
        return -1;
      }
      if (a.faculty < b.faculty) {
        return 1;
      }
      return 0;
    });
    listReplacer();
    clickCount = 0;
  }
});

tdDateOFBirth.addEventListener ('click', () => {
  ++clickCount;
  studentArrayFiltered.forEach (elem => elem.remove());
  if (clickCount == 1) {
    student.sort(function (a, b) {
      if (a.dateofbirth > b.dateofbirth) {
        return 1;
      }
      if (a.dateofbirth < b.dateofbirth) {
        return -1;
      }
      return 0;
    });
    listReplacer();
  }
  if (clickCount == 2) {
    student.sort(function (a, b) {
      if (a.dateofbirth > b.dateofbirth) {
        return -1;
      }
      if (a.dateofbirth < b.dateofbirth) {
        return 1;
      }
      return 0;
    });
    listReplacer();
    clickCount = 0;
  }
});

tdStartOfStudy.addEventListener ('click', () => {
  ++clickCount;
  studentArrayFiltered.forEach (elem => elem.remove());
  if (clickCount == 1) {
    student.sort(function (a, b) {
      if (a.startofstudy > b.startofstudy) {
        return 1;
      }
      if (a.startofstudy < b.startofstudy) {
        return -1;
      }
      return 0;
    });
    listReplacer();
  }
  if (clickCount == 2) {
    student.sort(function (a, b) {
      if (a.startofstudy > b.startofstudy) {
        return -1;
      }
      if (a.startofstudy < b.startofstudy) {
        return 1;
      }
      return 0;
    });
    listReplacer();
    clickCount = 0;
  }
});

formButtonFilter.addEventListener('click', (e) =>{
  e.preventDefault();
  studentFiltered = student.filter( data => {
    if (inputSurnameFilter.value && !data.surname.includes(inputSurnameFilter.value)) {
      return false;
    }
    if (inputNameFilter.value && !data.name.includes(inputNameFilter.value)) {
      return false;
    }
    if (inputMidnameFilter.value && !data.midname.includes(inputMidnameFilter.value)) {
      return false;
    }
    if (inputFacultyFilter.value && !data.faculty.includes(inputFacultyFilter.value)) {
      return false;
    }
    if (inputDateOfBirthFilter.value && data.dateofbirth != inputDateOfBirthFilter.value) {
      return false;
    }
    if (inputStartOfStudyFilter.value && data.startofstudy != inputStartOfStudyFilter.value) {
      return false;
    }
    return true;
  })
  listReplacerFilter();
});
});
